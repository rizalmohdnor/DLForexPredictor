---
title: "Forex Data for EURUSD60"
output:
  html_document: default
  html_notebook: default
---

This Project takes the exchange rate history and generate data for 10 inputs feature for ML. The input data are 

1. Raw Data
    + Exchange Rate
    + Top Price
    + Low Price
    + Closing Price
1. Moving Average (9 Points)
1. Relative Strength Index (RSI)
1. Stochastic RSI
    + Slow stochastic
    + Fast stochastic
1. William %R

Part 1: Getting the RAW EURUSD data and plot it to see how it looks like

```{r}
# Identifying excel sheets in file
filename <- 'EURUSD60.csv'
eurusd60 <- read.csv(filename)
library(dplyr)
eurusd60$newdate <- with(eurusd60,as.POSIXct(paste(DATE, TIME), format="%Y.%m.%d %H:%M"))
library(ggplot2)
require(scales)
g1 = ggplot(eurusd60, aes(newdate, CLOSE)) + geom_point(size=0.1) + geom_line() + scale_x_datetime(breaks=date_breaks("52 week"), minor_breaks=date_breaks("1 year")) + ggtitle("Open prices vs. Time") 
g1

```  

Part 2: Generate the Moving Average with n=9 points

We should try using Double Exponential moving average (dema)and try others later.
https://www.rdocumentation.org/packages/TTR/versions/0.23-2/topics/SMA
```{r}
library(TTR)
eurusd60$ema <-   EMA(eurusd60$CLOSE, 9) #Exponential moving average.
eurusd60$dema <-   DEMA(eurusd60$CLOSE, 9) #Weighted moving average.
eurusd60$evwma <-   EVWMA(eurusd60$CLOSE,eurusd60$VOLUME, 9) #Elastic, volume-weighted moving average.
eurusd60$zlema <-   ZLEMA(eurusd60$CLOSE, 9) #Zero lag exponential moving average.
#eurusd60$alma <-   ALMA(eurusd60$CLOSE, 9)
#eurusd60$hma <-   HMA(eurusd60$CLOSE, 9) #

g2 = ggplot(eurusd60, aes(newdate, y=ema)) + geom_point(size=0.1) +
  geom_line(aes(y=CLOSE,colour = "CLOSE")) +
  geom_line(aes(y=ema,colour = "Exponential moving average")) +
  geom_line(aes(y=dema,colour = "Double-exponential moving average")) + 
  geom_line(aes(y=evwma,colour = "Elastic, volume-weighted moving average")) +
  geom_line(aes(y=zlema,colour = "Zero lag exponential moving average.")) 
#+ scale_x_datetime(breaks=date_breaks("52 week"), minor_breaks=date_breaks("1 year"))
min <- as.POSIXct(paste("2017.09.07", "12:00"), format="%Y.%m.%d %H:%M")
max <- max(eurusd60$newdate)
g2 + 
  scale_x_datetime(limits = c(min, max)) + 
  ylim(1.193,1.209) + 
  xlab("Date") + 
  ylab("CLOSE Price") + 
  ggtitle("Standard Moving Average CLOSE prices vs. Time") 


```  

Part 3: Generate RSI (Relating Strength Index) based on Open Price. There are many types as in the example https://www.rdocumentation.org/packages/TTR/versions/0.23-2/topics/RSI. Not sure of the differences, seems like only default make sense. Please use the default (drsi).

```{r}
require(TTR)
eurusd60$drsi <- RSI(eurusd60$CLOSE)
# # Case of one 'maType' for both MAs
# eurusd60$rsima1 <- RSI(eurusd60$CLOSE, n=14, maType="WMA", eurusd60$VOLUME)
# # Case of two different 'maType's for both MAs
# eurusd60$rsima2 <- RSI(eurusd60$CLOSE, n=14,maType=list(maUp=list(EMA,ratio=1/5),
#              maDown=list(WMA,wts=1:10)))

# g3 = ggplot(eurusd60, aes(newdate, y=drsi)) + geom_point(size=0.1) +
# #  geom_line(aes(y=CLOSE,colour = "CLOSE")) +
#   geom_line(aes(y=drsi,colour = "RSI default")) 
#   geom_line(aes(y=rsima1,colour = "RSI MA1")) +
#   geom_line(aes(y=rsima2,colour = "RSI MA2")) 
# min <- as.POSIXct(paste("2017.09.08", "12:00"), format="%Y.%m.%d %H:%M")
# max <- max(eurusd60$newdate)
# g3 + 
#   scale_x_datetime(limits = c(min, max)) + 
#   ylim(25,85) + 
#   xlab("Date") + 
#   ylab("RSI Default") + 
#   ggtitle("RSI from Open prices vs. Time") 


```

Part 4: Generate Slow and Fast Stochastic RSI (Relating Strength Index) based on RSI. There are many types as in the example https://www.rdocumentation.org/packages/TTR/versions/0.23-2/topics/stoch. Not sure of the differences. Need to figure this out
```{r}
stochrsi <- stoch(RSI(eurusd60[,"CLOSE"]) )
eurusd60 <- cbind(eurusd60,stochrsi)
# g4 = ggplot(eurusd60, aes(newdate, y=fastK)) + geom_point(size=0.1) +
#   geom_line(aes(y=fastK,colour = "Fast RSI")) +
#   geom_line(aes(y=fastD,colour = "SLOW RSI")) 
#   geom_line(aes(y=slowD,colour = "RSI MA2")) 
# min <- as.POSIXct(paste("2017.09.08", "12:00"), format="%Y.%m.%d %H:%M")
# max <- max(eurusd60$newdate)
# g4
# g4 + 
#   scale_x_datetime(limits = c(min, max)) + 
# #  ylim(25,85) + 
# #  xlab("Date") + 
# #  ylab("value") + 
# #  ggtitle("RSI from Open prices vs. Time") 
```
Part 5. Generate the William R%

```{r}
eurusd60$wpr <- WPR(eurusd60[,c("HIGH","LOW","CLOSE")])
# 
# plot(tail(stochOsc[,"fastK"], 100), type="l",
#     main="Fast %K and Williams %R", ylab="",
#     ylim=range(cbind(stochstochRSI <- stoch( RSI(ttrc[,"Close"]) )Osc, stochWPR), na.rm=TRUE) )
# lines(tail(stochWPR, 100), col="blue")
# lines(tail(1-stochWPR, 100), col="red", lty="dashed")
```
Rearrange the columns and create new frames for _training_ and _validation_

save data to excel, currently being commented out
library(xls)x
write.xlsx(eurusd60, "/home/joey/eurusd.xlsx") # as excel
write.table(mydata, "c:/mydata.txt", sep="\t") # as csv
```{r}
eurusd60 <- eurusd60[,c(1,2,8,3:7,9:17)]
trainingset =  eurusd60[32:10408,c(3:17)]
validationset = eurusd60[10408:17378,c(3:17)]

#library(xlsx)
#write.xlsx(eurusd60, "/home/joey/eurusd.xlsx")
```