import csv
import sys
from datetime import datetime, timedelta

f1 = open(sys.argv[1], 'rb') #the EA file
readerEA = csv.reader(f1)
f2 = open(sys.argv[2], 'rb') #the forex file
reader = csv.reader(f2)
forex = sys.argv[3]
ticks = int(sys.argv[4])

row = []
count = 1
countea =1
match = 0
for row in reader:
    if (count == 1):
        print row
        count+= 1
        continue
    date_str = row[0] + ' ' + row[1]
    date = datetime.strptime(date_str, '%Y.%m.%d %H:%M')
    f1.seek(0)
    match=0
    for row2 in readerEA:
        if(row2[0] != 'Ticket'): 
            dateEAopen = datetime.strptime(row2[1], '%Y.%m.%d %H:%M:%S')
            dateEAclose = datetime.strptime(row2[8], '%Y.%m.%d %H:%M:%S')
            if (date<dateEAopen<date+timedelta(minutes=60) and row2[4]=='eurusd'):
                print date, row[2], row[3], row[4], row[5], row[6], row2[2], row2[3], row2[4], row2[5],row2[6], row2[7],row2[9], row2[10], row2[11],row2[12],row2[13], dateEAopen, dateEAclose, dateEAclose-dateEAopen
                match = 1
            else:
                match = 0
    if (match!=1):
        print date, row[2], row[3], row[4], row[5], row[6], 'idle', 'NULL','NULL','NULL','NULL','NULL','NULL','NULL','NULL','NULL','NULL','NULL','NULL','NULL', 'NULL', 'NULL'            
    if (match==1):
        continue
    
f1.close()
f2.close()
