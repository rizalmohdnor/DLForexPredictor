# This is research work using Deep Learning Forex Predictor

Work done is base on R and the file program1.Rmd is the R code to generate data, analyze and generate graphs. The data is also included.

This Project takes the exchange rate history and generate data for 10 inputs feature for Deep Learning and possibly more inputs. For now we are only considering 10 inputs.

The input data are 
1. Raw Data     
   + Exchange Rate     
   + Top Price     
   + Low Price     
   + Closing Price 
1. Moving Average (9 Points) 
1. Relative Strength Index (RSI) 
1. Stochastic RSI     
   + Slow stochastic
   + Fast stochastic 
1. William %R

# Then we will try to execute DL
